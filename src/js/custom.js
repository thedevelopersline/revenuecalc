var interval;
var intervalFinish;
var intervalSeconds;

function drawCounter(percent, counter) {
    $('div.counter').html('<div class="percent-wrapper"><div class="percent"></div></div><div id="slice"' + (percent > 50 ? ' class="gt50"' : '') + '><div class="pie"></div>' + (percent > 50 ? '<div class="pie fill"></div>' : '') + '</div>');
    var deg = 360 / 100 * percent;
    $('#slice .pie').css({
        '-moz-transform': 'rotate(' + deg + 'deg)',
        '-webkit-transform': 'rotate(' + deg + 'deg)',
        '-o-transform': 'rotate(' + deg + 'deg)',
        'transform': 'rotate(' + deg + 'deg)'
    });
    $('.percent').html(counter.toFixed(2));
}

function stopWatch(count) {
    if (count == 0) {
        clearInterval(interval);
        drawCounter(0, 0);
        return;
    }

    var seconds = (intervalFinish - (new Date().getTime())) / 1000;
    if (seconds <= 0) {
        drawCounter(100, count);
        clearInterval(interval);
        $('input[type=button]#watch').val('Start');
        //        alert('Finished counting down from '+intervalSeconds);
    } else {
        var percent = 100 - ((seconds / intervalSeconds) * 100);
        var counter = count - ((seconds / intervalSeconds) * count);
        drawCounter(percent, counter);
    }
}

function startCounter(count) {
    intervalSeconds = 3;
    intervalFinish = new Date().getTime() + (intervalSeconds * 1000);
    if (interval) {
        clearInterval(interval);
    }
    interval = setInterval('stopWatch(' + count + ')', 50);
}