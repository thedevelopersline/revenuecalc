angular.module('EanRevCalc.controllers.Main', [])

.controller('navbarCtrl', ['$scope', '$location', function($scope, $location, hmTouchEvents){
    
    $scope.goHome = function(event){
        if (typeof event !== "undefined" && typeof event.type !== "undefined")
            alert(event.type);

        if ($location.path() != '/')
            $location.path('/');
        else
            $location.url('/?foo=' + Math.random(0,9999));
    }

    $scope.goUser = function(event){
        if (typeof event !== "undefined" && typeof event.type !== "undefined")
            alert(event.type);

        $location.url('/user');
    }

}])

.controller('MainController', ['$scope', '$timeout', 'Data', 'DataManager', '$document','$routeParams',
    function ($scope, $timeout, Data, DataManager, $document, $routeParams, $rootScope) {
        $scope.Data = Data;

        $scope.showCares = false;

        $scope.searchVisibility = false;

        $scope.selectedCategory = null;
        $scope.categoryChanging = false;

        var _timeout;

        $scope.$watch("search", function (value) {
            if (typeof value != 'undefined') {
                $scope.showCares = true;
                $scope.filter = value;
            }
            if (value == null || value == "")
                $scope.showCares = false;
        });

        $scope.showCategory = function ($event, id) {

            var timer = ( $scope.selectedCategory == null ) ? 20 : 400;

            if (_timeout)
                return;
            $scope.categoryChanging = true;
            _timeout = $timeout(function () {
                $scope.selectedCategory = $scope.Data.Categories[id];
                $scope.categoryChanging = false;
                $timeout.cancel(_timeout);
                _timeout = null;
            }, timer);

        }

        $scope.showCategories = function () {
            $scope.showCares = false;
        }

        $scope.toggleSearch = function () {
            $scope.searchVisibility = $scope.searchVisibility ? false : true;
        }

        $scope.selectCategory = function (category) {
            $scope.showCares = true;
            $scope.filter = {
                "category": category.id
            }
        }

        $scope.selectBaseCare = function(x){
            DataManager.addBaseCare(x);
            DataManager.addCare();
        }

        $scope.showCareDetail = function (care) {
            if ($scope.selected == care.name)
                $scope.selected = null;
            else{
                var getName = (function (care) {
                        var carename = care;
                        return function(){
                            console.log(carename);
                            return carename;
                        }
                  })(care.name);

                $scope.selected = getName();
            }
                 
        }
}])
.controller('userButtonCtrl', ['DataManager', '$scope', function (DataManager, $scope) {
        $scope.selectedCares = DataManager.selectedCares;
}])
.controller('CareController', ['DataManager', '$scope', function (DataManager, $scope) {

        $scope.selectedCares = DataManager.getSelectedCares();

        $scope.Count = DataManager.getTotalRevenue();

        $scope.Reset = function(){
            DataManager.reset();
            //$scope.selectedCares = [];
            $scope.Count = 0;
            startCounter(0);
        }

        $scope.removeListing = function(listing){
            DataManager.removeListing(listing);
            startCounter( DataManager.getTotalRevenue() );
        }
}])

.controller('ContactController', ['$scope', function($scope){



}])

.controller('careDetailController', ['$scope', '$routeParams', 'DataManager', '$rootScope', function ($scope, $routeParams, DataManager, $rootScope) {
    // if view is accessed directly
    if (typeof $routeParams.careId != 'undefined') {
        var careId = $routeParams.careId;
        Care = DataManager.findCareById(careId);
        if (Care != false)
            $scope.selectedCare = Care;
        else
            $scope.notFound = true;
    }

    $scope.selectCare = function (care, price, detail) {
        $scope.$parent.$parent.selected = null;

        DataManager.checkoutCare(care, price, detail);

        if(care.baseCare){
            $rootScope.Ui.turnOn('modal1');
        }else{
            DataManager.addBaseCare(false);
            DataManager.addCare();
        }

    }

}])

.controller('swipeTestController', ['$scope', function ($scope) {
    $scope.fn1 = function () {

    }
}])

.controller('counterController', ['$scope', function ($scope) {

    $scope.display = 0;

    var animationLength = 2000;
    var interval = 150;
    var step = ($scope.count * interval) / animationLength;
    if (step == 0) step = 1000;

     startCounter($scope.count);

}])