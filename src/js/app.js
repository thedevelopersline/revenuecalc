angular.module('EanRevCalc', [
  'ngRoute',
  'ngAnimate',
  'mobile-angular-ui',
  'EanRevCalc.controllers.Main',
  'EanRevCalc.services',
  'EanRevCalc.filters',
  'revenueCalc.directives',
  'mobile-angular-ui.gestures'
])
.config(function($routeProvider, $compileProvider) {
    //wp8 fix
  $compileProvider.imgSrcSanitizationWhitelist('images/');

  $routeProvider.when('/', {templateUrl:'home.html', controller: 'MainController', reloadOnSearch: true})
  .when('/user', {templateUrl: 'userSummary.html', controller: 'CareController'})
  .when('/care/:careId', {templateUrl: 'careDetail.html', controller: 'careDetailController'})
  .when('/swipeTest', {templateUrl: 'swipeTest.html', controller: 'swipeTestController'})
  .when('/contact', {templateUrl: 'contact.html', controller: 'ContactController'})
})
.run(['$rootScope', function($rootScope){
/*    if(typeof device != 'undefined')
        alert(device.platform);*/
        //console.log(device);

//        http://docs.phonegap.com/en/1.2.0/phonegap_device_device.md.html#Device
}]);