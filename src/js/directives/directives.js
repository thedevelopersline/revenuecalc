angular.module('revenueCalc.directives', []).directive('counterDirective', [function(){
    // Runs during compile
    return {
        // name: '',
        // priority: 1,
        // terminal: true,
        scope: {
            count: '='
        }, // {} = isolate, true = child, false/undefined = no change
        controller: 'counterController',
        // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
        // restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
        template: '<div class="counter-wrapper"><ul class="flip-counter default" id="amountCounter"></ul></div>',
        // templateUrl: '',
        // replace: true,
        // transclude: true,
        // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
        link: function($scope, iElm, iAttrs, controller) {
        }
    };
}])
.directive('careDetails', [function(){
    // Runs during compile
    return {
        // name: '',
        // priority: 1,
        // terminal: true,
        scope: {
            "selectedCare" : "=care"
        }, // {} = isolate, true = child, false/undefined = no change
        controller: "careDetailController",
        // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
        // restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
        // template: '',
        templateUrl: 'careDetail.html',
        replace: false
        // transclude: true,
        // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
/*        link: function($scope, iElm, iAttrs, controller) {
            
        }*/
    };
}])
.directive('pricingDirective', ['$compile', function($compile){
    // Runs during compile
    return {
        // name: ''
        // terminal: true,
        scope: {
            listing: '='
        }, // {} = isolate, true = child, false/undefined = no change
        // controller: function($scope, $element, $attrs, $transclude) {},
        // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
        //restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
        // template: '',
        // templateUrl: '',
        // replace: true,
        // transclude: true,
        // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
        link: function($scope, element, iAttrs, controller) {
            
            $scope.$watch('listing', function(listing){
                if ( listing.baseCare != false){
                    $scope.baseAmount = ( listing.detail.length > 2) ? 4.05 : 2.95;
                    element.html("<ul class='priceList'><li><b>€ {{ listing.pricing }}</b></li><li><b>€ {{ baseAmount }}</b>(basis)</li></ul>");
                }else
                    element.html("<ul class='priceList'><li><b>€ {{ listing.pricing }}</b></li></ul>");

                $compile(element.contents())($scope);

            });

        }
    };
}])
.directive('focusMe', ['$timeout', '$parse', function($timeout, $parse) {
  return {
    //scope: true,   // optionally create a child scope
    priority: -1,
    link: function(scope, element, attrs) {
      var model = $parse(attrs.focusMe);
      scope.$watch(model, function(value) {
        console.log('value=',value);
        if(value == true) { 
          $timeout(function() {
            element[0].focus(); 
          });
        }
      });
      // to address @blesh's comment, set attribute value to 'false'
      // on blur event:
      element.bind('blur', function() {
         console.log('blur');
         scope.$apply(model.assign(scope, false));
      });
    }
  };
}]);