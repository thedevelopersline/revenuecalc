angular.module('EanRevCalc.filters', [])
.filter('careDetail', function(){
    return function(input){
        var first = (input.substring(0,1) == "i") ? "Verhoogde tegemoedkoming, " : "Normaal tarief, ";
        var weekendORnot = (input.length > 2) ? "weekendtarief" : "weektarief";
        
        return first + weekendORnot;
    }
});